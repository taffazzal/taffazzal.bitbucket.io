class Timeline {
    constructor({
        period = '',
        contents = [{
            text: '',
            extra_text: ''
        }]
    }) {
        this.period = period;
        this.contents = contents;
    }
}

let timelineContents = [
    new Timeline({
        period: '2016 - 2017',
        contents: [{
                text: 'Graduated from University of Greenwich with First Class BSc Honors in Computer Science.'
            },
            {
                text: 'Became director for Creativebot Agency and started working as a freelancer.',
                extra_text: 'Skills and Tools used: Wordpress, HTML5, CSS3(SASS Preprocessor), JavaScript, Typescript, PHP, MySQL, Responsive Web Designs'
            },
            {
                text: 'Resigned from Creativebot Agency as director and went into internship with Skills Hive.',
                extra_text: 'Skills and Tools used: Wordpress, Woocommerce, HTML5, CSS3(SASS Preprocessor), JavaScript, jQuery, PHP, MySQL, Responsive Web Designs'
            }
        ]
    }),
    new Timeline({
        period: '2017 - 2020',
        contents: [{
            text: 'Started working for Peoples Systems Limited agency as a contractor once internship finished.',
            extra_text: 'Skills and Tools used: Angular, Ionic, NativeScript, Couchbase Server, Couchbase Lite, HTML5, CSS3(SASS Preprocessor), JavaScript, Typescript, Node.js(Express JS), Phaser3, MongoDB, NGXS State Management, Responsive Web Designs'
        }]
    })
]
class Project {
    constructor({
        nickname = '',
        title = '',
        description = '',
        company = '',
        image = '',
        url = ''
    }) {
        this.nickname = nickname;
        this.title = title;
        this.description = description;
        this.company = company;
        this.image = image;
        this.url = url;
    }
}

let projects = [
    new Project({
        title: 'TickAudit',
        description: 'TickAudit is a platform enabling companies to manage their Health, Safety, Environment and other business related procedures, inspections, and incident reporting electronically in real-time. The dashboard is a single page app built with Angular and Node.js with Couchbase Server as the database.<br/><br/>Audits are carried out with an mobile app (built with Ionic and Couchbase Lite) and can be done offline as well. Once it\'s back online, the data is automatically synced to the backend database.',
        company: 'Peoples Systems Limited',
        image: './assets/images/tickaudit.png',
        url: 'https://tickaudit.com/'
    }),
    new Project({
        title: 'Catchy Tunes',
        description: 'Catchy Tunes is an HTML5 mobile game to encourage children to learn music interactively with the use of gamification techniques. Built with Phaser (using Typescript) and wrapped around Ionic to allow running of the app in iOS/Android.<br/><br/>This is an initial version of the game with basic functionalities. More features are to be added in subsequent versions.',
        company: 'Peoples Systems Limited',
        image: './assets/images/catchytunes.png',
        url: ''
    }),
    new Project({
        title: 'Start Safe',
        description: 'Start Safe is powered by TickAudit platform but it is catered to provide a fully auditable system to carry out pre-start checks on any type of vehicle. Audits are carried out with an mobile app.',
        company: 'Peoples Systems Limited',
        image: './assets/images/startsafe.png',
        url: 'https://dashboard.startsafe.tech/login'
    }),
    new Project({
        title: 'Battleship Online',
        description: 'Battleship is an ambitious multiplayer online game where players can play with other players. Due to time constraint, I was unable to complete this project and only managed to work on the front-end side of it. My ideal tech stack would have been to use Angular, Node.js and Socket.IO. </br></br>Perhaps I may return to this project one day to finish it off :)',
        company: 'Personal Side Project',
        image: './assets/images/battleship.png',
        url: ''
    }),
    new Project({
        title: 'Masjid Humera',
        description: 'Masjid Humera is an admin dashboard that allows the admin to fill in the prayer times for the upcoming years which will then automatically sync back to the mobile app. Notification can be sent from the dashboard to the app to notify users of upcoming events/classes. Dashboard built with Angular and mobile app built with Ionic with Firebase database.',
        company: 'Personal Side Project',
        image: './assets/images/humera.png',
        url: ''
    }),
    new Project({
        title: 'Butlers',
        description: 'Butlers is an e-commerce and informative site that deals with arboricultural. A Wordpress hosted site (with custom theme) written in PHP and Javascript with WooCommerce integration. ',
        company: 'Skills Hive',
        image: './assets/images/butlers.png',
        url: 'https://lopit.co.uk/'
    }),
    new Project({
        title: 'Beastmode Academy',
        description: 'Beastmode Academy is an fitness and e-commerce site that allows users to book courses online for their next fitness training and shop for Beastmode clothing brands. A Wordpress hosted site (with custom theme) written in PHP with WooCommerce integration.',
        company: 'Creativebot Agency',
        image: './assets/images/beastmode.png',
        url: ''
    }),
    new Project({
        nickname: 'RCH Admin',
        title: 'Research Center for Hadith',
        description: 'RCH is an admin dashboard that allows the admin to add, edit, delete and update Islamic books. An companion mobile app (built with Ionic) allows the users to view these books and download them to their device. The dashboard was built with jQuery, PHP, HTML and CSS3 with Firebase database.',
        company: 'Personal Side Project',
        image: './assets/images/rch.png',
        url: ''
    }),
    new Project({
        nickname: 'Diamond TDC',
        title: 'Diamond Tailors & Dry Cleaning',
        description: 'Diamond TDC is an tailoring and dry cleaning website offering wide variety of services. Built with jQuery, HTML and CSS3.<br/><br/>This site is a static website so no dynamic content.',
        company: 'Creativebot Agency',
        image: './assets/images/diamond.png',
        url: 'https://diamondtdc.co.uk/'
    })
]
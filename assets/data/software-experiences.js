class SoftwareExperience {
    constructor({
        name = '',
        classes = [],
        subcategories = [{
            title: '',
            percentage: 0
        }]
    }) {
        this.name = name;
        this.classes = classes;
        this.subcategories = subcategories;
    }
}

let softwareExperiences = [
    new SoftwareExperience({
        name: 'Languages',
        classes: ['languages-category', 'mt-3'],
        subcategories: [{
                title: 'HTML5',
                percentage: 0.90
            },
            {
                title: 'CSS3',
                percentage: 0.85
            },
            {
                title: 'Javascript',
                percentage: 0.75
            },
            {
                title: 'Typescript',
                percentage: 0.75
            },
            {
                title: 'PHP',
                percentage: 0.70
            },
            {
                title: 'C#',
                percentage: 0.55
            },
        ]
    }), new SoftwareExperience({
        name: 'Frameworks',
        classes: ['frameworks-category', 'mt-4'],
        subcategories: [{
                title: 'Angular',
                percentage: 0.75
            },
            {
                title: 'Ionic',
                percentage: 0.75
            },
            {
                title: 'NativeScript',
                percentage: 0.70
            },
            {
                title: 'NestJS',
                percentage: 0.75
            },
            {
                title: 'Wordpress',
                percentage: 0.80
            },
            {
                title: 'Express.js',
                percentage: 0.75
            },
            {
                title: 'Phaser',
                percentage: 0.65
            }
        ]
    }), new SoftwareExperience({
        name: 'Databases',
        classes: ['databases-category', 'mt-4'],
        subcategories: [{
                title: 'MySQL',
                percentage: 0.70
            },
            {
                title: 'Couchbase',
                percentage: 0.85
            },
            {
                title: 'Firebase',
                percentage: 0.80
            },
            {
                title: 'MongoDB',
                percentage: 0.60
            }
        ]
    }), new SoftwareExperience({
        name: 'Project Management',
        classes: ['project-management-category', 'mt-4'],
        subcategories: [{
                title: 'Trello',
                percentage: 0.95
            },
            {
                title: 'Jira',
                percentage: 0.75
            },
            {
                title: 'Git',
                percentage: 0.80
            }
        ]
    })
];
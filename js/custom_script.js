let navbar = document.getElementById("navbar");
let navbarSticky = navbar.offsetTop;
let navbarLinks = document.getElementById('page-links');
let projectModal = document.getElementById("project-modal");
let navbarHeight = navbar.clientHeight;

let isIos = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
let isSafari = !!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/);

let changeAnimation = false;

initialiseTimelineContent();
initialiseSoftwareExperiences();
initialiseProjects();
getYear();
checkIfMobile();
fixNavbarToTop();
changeNavbarActiveStatusWhenScrolling();

function initialiseTimelineContent() {
    for (let i = 0; i < timelineContents.length; i++) {
        let timelineContent = timelineContents[i];

        let contentWrapperNode = document.createElement('div');
        contentWrapperNode.classList.add('content-wrapper', 'loser');

        let contentNode = document.createElement('div');
        contentNode.classList.add('content');

        let periodNode = document.createElement('h4');
        let periodTextNode = document.createTextNode(timelineContent.period);
        periodNode.appendChild(periodTextNode);
        contentNode.appendChild(periodNode);

        for (let content of timelineContent.contents) {
            let paragraphNode = document.createElement('p');
            let paragraphTextNode = document.createTextNode(content.text);
            paragraphNode.appendChild(paragraphTextNode);

            if (content.extra_text && content.extra_text.length > 0) {
                let extraNode = document.createElement('span');
                extraNode.classList.add('content-extra-info', 'mt-2');
                let extraTextNode = document.createTextNode(content.extra_text);
                extraNode.appendChild(extraTextNode);
                paragraphNode.appendChild(extraNode);
            }
            contentNode.appendChild(paragraphNode);
        }
        contentWrapperNode.appendChild(contentNode);
        document.getElementById('timeline').appendChild(contentWrapperNode);
    }
}

function initialiseSoftwareExperiences() {
    for (let i = 0; i < softwareExperiences.length; i++) {
        let softwareExperience = softwareExperiences[i];

        let parentDivNode = document.createElement('div');
        parentDivNode.classList.add(...softwareExperience.classes);

        let nameNode = document.createElement('h6');
        let nameTextNode = document.createTextNode(softwareExperience.name);
        nameNode.appendChild(nameTextNode);

        parentDivNode.appendChild(nameNode);

        let unorderdListNode = document.createElement("ul");
        unorderdListNode.classList.add('subcategory', 'mt-2');

        for (let j = 0; j < softwareExperience.subcategories.length; j++) {
            let subcategory = softwareExperience.subcategories[j];

            let listNode = document.createElement("li");

            let titleNode = document.createElement("span");
            titleNode.classList.add('title');
            let titleTextNode = document.createTextNode(subcategory.title);
            titleNode.appendChild(titleTextNode);

            let progressNode = document.createElement('div');
            progressNode.classList.add('progress');
            progressNode.setAttribute('data-aos', 'progress');
            progressNode.setAttribute('data-aos-delay', '0');
            progressNode.setAttribute('data-aos-duration', '2000');
            progressNode.setAttribute('data-aos-once', 'true');
            progressNode.setAttribute('data-aos-anchor-placement', 'bottom-bottom');

            let remainingPercentNode = document.createElement('span');
            remainingPercentNode.classList.add('remaining-percent');
            remainingPercentNode.setAttribute('style', 'width: ' + (parseInt((1 - subcategory.percentage) * 100)) + '%');

            let percentageNode = document.createElement('span');
            percentageNode.classList.add('percentage');
            let percentageTextNode = document.createTextNode(parseInt(subcategory.percentage * 100) + '%');
            percentageNode.appendChild(percentageTextNode);

            progressNode.appendChild(remainingPercentNode);
            listNode.innerHTML += titleNode.outerHTML + progressNode.outerHTML + percentageNode.outerHTML;
            unorderdListNode.appendChild(listNode);
        }
        parentDivNode.appendChild(unorderdListNode);
        document.getElementById('software-experiences').appendChild(parentDivNode);
    }
}

function initialiseProjects() {
    for (let i = 0; i < projects.length; i++) {
        let project = projects[i];

        let projectNode = document.createElement('div');
        projectNode.classList.add('project', 'col-md-4', 'col-sm-6', 'col-xs-12');
        projectNode.setAttribute('data-aos', getProjectAnimationName(i));
        projectNode.setAttribute('data-aos-delay', '300');
        projectNode.setAttribute('data-aos-duration', '1500');
        projectNode.setAttribute('data-aos-once', 'true');

        let imageWrapperNode = document.createElement('div');
        imageWrapperNode.classList.add('image-wrapper');

        let imageNode = document.createElement('img');
        imageNode.src = project.image;
        imageNode.style.maxWidth = '100%';
        imageNode.alt = project.title + '-Image';

        let projectInfoNode = document.createElement('div');
        projectInfoNode.classList.add('project-info');
        let titleNode = document.createElement("h4");
        let titleTextNode = document.createTextNode(project.nickname && project.nickname.length > 0 ? project.nickname : project.title);
        titleNode.appendChild(titleTextNode);
        let buttonNode = document.createElement('div');
        buttonNode.classList.add('button', 'view-project');
        buttonNode.setAttribute('onclick', 'openModal("' + project.title + '")')
        let buttonTextNode = document.createTextNode('Learn More ');
        buttonNode.appendChild(buttonTextNode);
        let iconNode = document.createElement('i');
        iconNode.classList.add('fa', 'fa-arrow-right');
        buttonNode.appendChild(iconNode);

        projectInfoNode.innerHTML = titleNode.outerHTML + buttonNode.outerHTML;
        imageWrapperNode.innerHTML = imageNode.outerHTML + projectInfoNode.outerHTML;
        projectNode.appendChild(imageWrapperNode);
        document.getElementById('projects-list').appendChild(projectNode);
    }
}

function getProjectAnimationName(index = 0) {
    if (window.innerWidth < 576) {
        let counter = index + 1;
        if (counter && (counter % 2) === 0) {
            return 'fade-right';
        } else {
            return 'fade-left';
        }
    } else {
        if (index && (index % 3 === 0)) {
            changeAnimation = !changeAnimation;
        }
        if (changeAnimation) {
            return 'fade-left';
        } else {
            return 'fade-right';
        }
    }
}

function toggleMenu() {
    if (navbarLinks.classList.contains('show')) {
        navbarLinks.classList.remove('show');
    } else {
        navbarLinks.classList.add('show');
    }
}

function scrollToView(id, event) {
    if (event) {
        changeNavbarActiveStatusWhenScrolling(id);
    }

    let element = document.getElementById(id);
    let offset;
    if (navbar.classList.contains('mobile')) {
        offset = element.offsetTop - navbarHeight;
    } else {
        offset = element.offsetTop + (event && id === 'about' ? 0 : -(navbarHeight - 1));
    }
    window.scrollTo({
        top: offset,
        behavior: 'smooth'
    });
    if (event && navbar.classList.contains('mobile')) {
        toggleMenu();
    }
}

function fixNavbarToTop() {
    if (window.pageYOffset >= navbarSticky && !navbar.classList.contains('mobile')) {
        navbar.classList.add("fixed")
    } else {
        navbar.classList.remove("fixed");
    }
}

function checkIfMobile() {
    if (window.innerWidth < 576) {
        navbar.classList.add('mobile');
    } else {
        navbar.classList.remove('mobile');
    }
}

function openModal(name = '') {
    let data = getData(name);
    let image = document.querySelector('#project-modal .image-wrapper');
    let title = document.querySelector('#project-modal .project-title');
    let description = document.querySelector('#project-modal .project-description');
    let company = document.querySelector('#project-modal .project-company');
    let url = document.querySelector('#project-modal .project-url a');
    url.innerHTML = '';
    url.href = '';

    title.innerText = data.title;
    company.innerText = data.company;
    description.innerHTML = data.description;
    if (data.url && data.url.length > 0) {
        url.innerText = 'Visit Site';
        url.href = data.url;
    }

    image.style.background = 'url("' + data.image + '") no-repeat';
    image.style.backgroundSize = 'cover';
    document.body.classList.add('modal-open');

    if (isIos && isSafari) {
        bodyScrollLock.disableBodyScroll(projectModal);
    }
    projectModal.style.display = "block";
}

function getData(title = '') {
    return projects.find(project => project.title === title);
}

function changeNavbarActiveStatusWhenScrolling(id) {
    const pageLinks = document.querySelectorAll('.page-link');
    const sections = document.getElementsByClassName('section');
    const isMobile = navbar.classList.contains('mobile');

    for (let i = --pageLinks.length; i >= 0; i--) {
        let pageLink = pageLinks[i];
        if (window.pageYOffset >= (sections[i].offsetTop + (isMobile ? -navbarHeight : id && id === 'about' ? 0 : -(navbarHeight - 1)))) {
            if (!pageLink.classList.contains('active')) {
                pageLinks.forEach(link => link.classList.remove('active'));
                pageLink.classList.add('active');
            }
            break;
        }
    }
}

function closeModal() {
    document.body.classList.remove('modal-open');
    projectModal.style.display = "none";
    if (isIos && isSafari) {
        bodyScrollLock.enableBodyScroll(projectModal);
    }
}

function getYear() {
    let year = new Date().getFullYear();
    let yearEl = document.getElementById('year');
    yearEl.innerText += year;
}

window.onload = function () {
    AOS.init();
}

window.onresize = function () {
    checkIfMobile();
}

window.onscroll = function () {
    fixNavbarToTop();
    changeNavbarActiveStatusWhenScrolling();
};

window.onclick = function (event) {
    if (event.target == projectModal) {
        closeModal();
    }
}